<?php

/**
 * gallery_addon : gallery_common.inc
 */

/**
 * Function gallery_common_add_item().
 */
function gallery_common_add_item($item, $parent_id) {
  if (_gallery_init()) {
    // For non-numeric parent (i.e. 'default') get the root album
    if (!is_numeric($parent_id)) {
      list($ret, $parent_id) = GalleryCoreApi::getDefaultAlbumId();
      if ($ret) {
        gallery_error(t('Error trying to get default album id'), $ret);
        return FALSE;
      }
    }
    // Read lock parent item
    list($ret, $g2_lock_id) = GalleryCoreApi::acquireReadLock($parent_id);
    if (!$ret && $g2_lock_id) {
      // Add uploaded file to G2 album
      list($ret, $g2_item) = GalleryCoreApi::addItemToAlbum(
        $item['filepath'],
        $item['filename'],
        substr($item['filename'], 0, strrpos($item['filename'], '.')),
        isset($item['summary']) ? $item['summary'] : '',
        isset($item['description']) ? $item['description'] : '',
        $item['filemime'],
        $parent_id
      );
      // Release read lock
      GalleryCoreApi::releaseLocks($g2_lock_id);
      if ($ret) {
        gallery_error(t('Error adding uploaded item to album'), $ret);
        return FALSE;
      }
      
      return $g2_item;
    }
    else {
      gallery_error(t('Error trying to acquire read lock'), $ret);
      return FALSE;
    }
  }
  
  return FALSE;
}
