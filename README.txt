
Overview
---------
This package contains add-on modules for the Gallery2 integration module.
These modules extend the functionality of gallery module for tighter
integration with Drupal or interaction with other Drupal modules, i.e.
CCK, Views, OG, etc.
